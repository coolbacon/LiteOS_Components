diff --git a/thttpd-2.27/config.h b/thttpd-2.27/config.h
index 65ab1e3..758e55c 100644
--- a/thttpd-2.27/config.h
+++ b/thttpd-2.27/config.h
@@ -28,6 +28,18 @@
 #ifndef _CONFIG_H_
 #define _CONFIG_H_
 
+#define THTTPD_FOR_LITEOS (1)
+#define CONFIG_NSOCKET_DESCRIPTORS 20
+
+#ifdef THTTPD_FOR_LITEOS
+#define tdbg(fmt,arg...) do{dprintf("%s:%s:%d->",__FILE__,__FUNCTION__,__LINE__ );dprintf(fmt,##arg);}while(0)
+
+#define THTTP_WWWROOT_DIR "/fatfs/wwwroot"
+
+#define THTTPD_IPADDR (192<<24|168<<16|3<<8|92)
+#define THTTPD_PORT 80
+#define THTTPD_HOSTNAME "LiteOS_SERVER"
+#endif
 
 /* The following configuration settings are sorted in order of decreasing
 ** likelihood that you'd want to change them - most likely first, least
@@ -272,7 +284,9 @@
 /* CONFIGURE: nice(2) value to use for CGI programs.  If this is undefined,
 ** CGI programs run at normal priority.
 */
-#define CGI_NICE 10
+#ifdef CGI_NICE
+#undef CGI_NICE
+#endif
 
 /* CONFIGURE: $PATH to use for CGI programs.
 */
diff --git a/thttpd-2.27/fdwatch.c b/thttpd-2.27/fdwatch.c
index 2b3de74..fbd5992 100644
--- a/thttpd-2.27/fdwatch.c
+++ b/thttpd-2.27/fdwatch.c
@@ -30,7 +30,10 @@
 #include <unistd.h>
 #include <string.h>
 #include <sys/time.h>
+#include "config.h"
+#ifndef THTTPD_FOR_LITEOS
 #include <sys/resource.h>
+#endif
 #include <syslog.h>
 #include <fcntl.h>
 
@@ -38,6 +41,315 @@
 #define MIN(a,b) ((a) < (b) ? (a) : (b))
 #endif
 
+#if THTTPD_FOR_LITEOS
+#include <fcntl.h>
+#include <poll.h>
+
+#define poll_dbg(fmt,arg...)
+
+struct liteOS_poll_watch_s
+{
+  struct pollfd *pollfds;
+  void         **client;
+  unsigned char       *ready;
+  unsigned char        nfds;
+  unsigned char        nwatched;
+  unsigned char        nactive;
+  unsigned char        next;
+};
+
+static struct liteOS_poll_watch_s *gfw;
+#define WHICH                  "liteos_poll"
+#define INIT( nf )         poll_init( nf )
+#define ADD_FD( fd, cd )       poll_add_fd(gfw, fd, cd )
+#define DEL_FD( fd )           poll_del_fd(gfw, fd )
+#define WATCH( timeout_msecs ) poll_watch(gfw, timeout_msecs )
+#define CHECK_FD( fd )         poll_check_fd(gfw, fd )
+#define GET_FD_CLIENT_DATA( )         poll_get_client_data(gfw)
+
+static struct liteOS_poll_watch_s *poll_init(int nfds);
+static void poll_add_fd(struct liteOS_poll_watch_s *fw, int fd, void *client_data);
+static void poll_del_fd(struct liteOS_poll_watch_s *fw,int fd);
+static int poll_watch(struct liteOS_poll_watch_s *fw,long timeout_msecs);
+static int poll_check_fd(struct liteOS_poll_watch_s *fw,int fd);
+static void *poll_get_client_data(struct liteOS_poll_watch_s *fw);
+void poll_uninitialize(struct liteOS_poll_watch_s *fw);
+
+/* Routines. */
+
+/* Figure out how many file descriptors the system allows, and
+ ** initialize the fdwatch data structures.  Returns -1 on failure.
+ */
+int fdwatch_get_nfiles(void)
+{
+    int nfiles;
+    /* Figure out how many fd's we can have. */
+    nfiles = CONFIG_NSOCKET_DESCRIPTORS;
+
+    if ( (gfw=INIT( nfiles )) == NULL)
+        return -1;
+
+    return nfiles;
+}
+
+/* Add a descriptor to the watch list.  rw is either FDW_READ or FDW_WRITE.  */
+void fdwatch_add_fd(int fd, void* client_data, int rw)
+{
+    ADD_FD(fd, client_data);
+}
+
+/* Remove a descriptor from the watch list. */
+void fdwatch_del_fd(int fd)
+{
+    DEL_FD(fd);
+}
+
+/* Do the watch.  Return value is the number of descriptors that are ready,
+ ** or 0 if the timeout expired, or -1 on errors.  A timeout of INFTIM means
+ ** wait indefinitely.
+ */
+int fdwatch(long timeout_msecs)
+{
+    return WATCH(timeout_msecs);
+}
+
+/* Check if a descriptor was ready. */
+int fdwatch_check_fd(int fd)
+{
+    return CHECK_FD(fd);
+}
+
+void*
+fdwatch_get_next_client_data(void)
+{
+    return GET_FD_CLIENT_DATA();
+}
+
+/* Generate debugging statistics syslog message. */
+void fdwatch_logstats(long secs)
+{
+    if ( secs > 0 )
+    syslog(
+        LOG_NOTICE, "  fdwatch - %ld %ss (%g/sec)",
+        gfw->nwatched, WHICH, (float) gfw->nwatched / secs );
+}
+
+#ifdef THTTPD_POLL_WATCH_DEBUG
+
+static void poll_dump(const char *msg, FAR struct liteOS_poll_watch_s *fw)
+{
+  int i;
+
+  poll_dbg("%s\n", msg);
+  poll_dbg("nwatched: %d nfds: %d\n", fw->nwatched, fw->nfds);
+  for (i = 0; i < fw->nwatched; i++)
+  {
+      poll_dbg("%2d. pollfds: {fd: %d events: %02x revents: %02x} client: %p\n",
+           i+1, fw->pollfds[i].fd, fw->pollfds[i].events,
+           fw->pollfds[i].revents, fw->client[i]);
+  }
+  poll_dbg("nactive: %d next: %d\n", fw->nactive, fw->next);
+  for (i = 0; i < fw->nactive; i++)
+  {
+      poll_dbg("%2d. %d active\n", i, fw->ready[i]);
+  }
+}
+#else
+#define poll_dump(m,f)
+#endif
+
+static int poll_pollndx(struct liteOS_poll_watch_s *fw, int fd)
+{
+  int pollndx;
+
+  /* Get the index associated with the fd */
+
+  for (pollndx = 0; pollndx < fw->nwatched; pollndx++)
+    {
+      if (fw->pollfds[pollndx].fd == fd)
+        {
+          poll_dbg("pollndx: %d\n", pollndx);
+          return pollndx;
+        }
+    }
+
+  poll_dbg("No poll index for fd %d: %d\n", fd);
+  return -1;
+}
+
+/****************************************************************************
+ * Public Functions
+ ****************************************************************************/
+
+/* Initialize the fdwatch data structures.  Returns -1 on failure. */
+
+struct liteOS_poll_watch_s *poll_init(int nfds)
+{
+  struct liteOS_poll_watch_s *fw;
+
+  fw = (struct liteOS_poll_watch_s*)zalloc(sizeof(struct liteOS_poll_watch_s));
+  if (!fw)
+    {
+      poll_dbg("Failed to allocate fdwatch\n");
+      return NULL;
+    }
+
+  fw->nfds = nfds;
+
+  fw->client = (void**)zalloc(sizeof(void*) * nfds);
+  if (!fw->client)
+    {
+      goto errout_with_allocations;
+    }
+
+  fw->pollfds = (struct pollfd*)zalloc(sizeof(struct pollfd) * nfds);
+  if (!fw->pollfds)
+    {
+      goto errout_with_allocations;
+    }
+
+  fw->ready = (unsigned char*)zalloc(sizeof(unsigned char) * nfds);
+  if (!fw->ready)
+    {
+      goto errout_with_allocations;
+    }
+
+  poll_dump("Initial state:", fw);
+  return fw;
+
+errout_with_allocations:
+  poll_uninitialize(fw);
+  return NULL;
+}
+
+void poll_uninitialize(struct liteOS_poll_watch_s *fw)
+{
+  if (fw)
+    {
+      poll_dump("Uninitializing:", fw);
+      if (fw->client)
+        {
+          free(fw->client);
+        }
+
+      if (fw->pollfds)
+        {
+          free(fw->pollfds);
+        }
+
+      if (fw->ready)
+        {
+          free(fw->ready);
+        }
+
+      free(fw);
+    }
+}
+void poll_add_fd(struct liteOS_poll_watch_s *fw, int fd, void *client_data)
+{
+  poll_dbg("fd: %d client_data: %p\n", fd, client_data);
+  poll_dump("Before adding:", fw);
+
+  if (fw->nwatched >= fw->nfds)
+    {
+      poll_dbg("too many fds\n");
+      return;
+    }
+
+  fw->pollfds[fw->nwatched].fd     = fd;
+  fw->pollfds[fw->nwatched].events = POLLIN;
+  fw->client[fw->nwatched]         = client_data;
+
+  fw->nwatched++;
+  poll_dump("After adding:", fw);
+}
+
+void poll_del_fd(struct liteOS_poll_watch_s *fw, int fd)
+{
+  int pollndx;
+
+  poll_dbg("fd: %d\n", fd);
+  poll_dump("Before deleting:", fw);
+
+  pollndx = poll_pollndx(fw, fd);
+  if (pollndx >= 0)
+    {
+      fw->nwatched--;
+      if (pollndx != fw->nwatched)
+        {
+          fw->pollfds[pollndx] = fw->pollfds[fw->nwatched];
+          fw->client[pollndx]  = fw->client[fw->nwatched];
+        }
+    }
+   poll_dump("After deleting:", fw);
+}
+int poll_watch(struct liteOS_poll_watch_s *fw, long timeout_msecs)
+{
+  int ret;
+  int i;
+
+  poll_dump("Before waiting:", fw);
+  poll_dbg("Waiting... (timeout %d)\n", timeout_msecs);
+  fw->nactive = 0;
+  fw->next    = 0;
+  ret         = poll(fw->pollfds, fw->nwatched, (int)timeout_msecs);
+
+  if (ret > 0)
+    {
+      for (i = 0; i < fw->nwatched; i++)
+        {
+          if (fw->pollfds[i].revents & (POLLIN | POLLERR | POLLHUP | POLLNVAL))
+            {
+              poll_dbg("pollndx: %d fd: %d revents: %04x\n",
+                    i, fw->pollfds[i].fd, fw->pollfds[i].revents);
+
+              fw->ready[fw->nactive++] = fw->pollfds[i].fd;
+              if (fw->nactive == ret)
+                {
+                  break;
+                }
+            }
+        }
+    }
+  poll_dbg("nactive: %d\n", fw->nactive);
+  poll_dump("After wakeup:", fw);
+  return ret;
+}
+
+int poll_check_fd(struct liteOS_poll_watch_s *fw, int fd)
+{
+  int pollndx;
+
+  poll_dbg("fd: %d\n", fd);
+  poll_dump("Checking:", fw);
+
+  pollndx = poll_pollndx(fw, fd);
+  if (pollndx >= 0 && (fw->pollfds[pollndx].revents & POLLERR) == 0)
+    {
+      poll_dbg("fw->pollfds[pollndx].revents=%x\n",fw->pollfds[pollndx].revents);
+      return fw->pollfds[pollndx].revents & (POLLIN | POLLHUP | POLLNVAL);
+    }
+
+  poll_dbg("POLLERR fd: %d\n", fd);
+  return 0;
+}
+
+void *poll_get_client_data(struct liteOS_poll_watch_s *fw)
+{
+  poll_dump("Before getting client data:", fw);
+  if (fw->next >= fw->nwatched)
+    {
+      poll_dbg("All client data returned: %d\n", fw->next);
+      return (void*)-1;
+    }
+
+  poll_dbg("client_data[%d]: %p\n", fw->next, fw->client[fw->next]);
+  return fw->client[fw->next++];
+}
+
+
+#else
+
 #ifdef HAVE_POLL_H
 #include <poll.h>
 #else /* HAVE_POLL_H */
@@ -832,3 +1144,6 @@ select_get_fd( int ridx )
 # endif /* HAVE_DEVPOLL */
 
 #endif /* HAVE_KQUEUE */
+
+#endif
+
diff --git a/thttpd-2.27/libhttpd.c b/thttpd-2.27/libhttpd.c
index 3814e6a..44dee42 100644
--- a/thttpd-2.27/libhttpd.c
+++ b/thttpd-2.27/libhttpd.c
@@ -81,6 +81,7 @@
 #include "timers.h"
 #include "match.h"
 #include "tdate_parse.h"
+#include "lwip/netdb.h"
 
 #ifndef STDIN_FILENO
 #define STDIN_FILENO 0
@@ -115,6 +116,10 @@ typedef int socklen_t;
 #define MIN(a,b) ((a) < (b) ? (a) : (b))
 #endif
 
+#ifdef USE_IPV6
+#undef USE_IPV6
+#endif
+
 
 /* Forwards. */
 static void check_options( void );
@@ -181,7 +186,6 @@ static int my_snprintf( char* str, size_t size, const char* format, ... );
 static long long atoll( const char* str );
 #endif /* HAVE_ATOLL */
 
-
 /* This global keeps track of whether we are in the main process or a
 ** sub-process.  The reason is that httpd_write_response() can get called
 ** in either context; when it is called from the main process it must use
@@ -192,6 +196,53 @@ static long long atoll( const char* str );
 */
 static int sub_process = 0;
 
+int fork(void)
+{
+    PRINT_ERR("%s is not supported\n", __FUNCTION__);
+    errno = ENOSYS;
+    return -1;
+}
+
+int pipe(int pipedes[])
+{
+    (VOID)pipedes;
+    PRINT_ERR("%s is not supported\n", __FUNCTION__);
+    errno = ENOSYS;
+    return -1;
+}
+
+int chdir(const char *path)
+{
+    PRINT_ERR("%s is not supported\n", __FUNCTION__);
+    errno = ENOSYS;
+    return -1;
+}
+
+int execve(const char *file, char * const *argv, char * const *envp)
+{
+    (VOID)file;
+    (VOID)argv;
+    (VOID)envp;
+    PRINT_ERR("%s is not supported\n", __FUNCTION__);
+    errno = ENOSYS;
+    return -1;
+}
+
+void closelog(void)
+{
+    PRINT_ERR("%s is not supported\n", __FUNCTION__);
+    errno = ENOSYS;
+}
+
+char *crypt(const char *key, const char *salt)
+{
+    (VOID)key;
+    (VOID)salt;
+    PRINT_ERR("%s is not supported\n", __FUNCTION__);
+    errno = ENOSYS;
+    return NULL;
+}
+
 
 static void
 check_options( void )
@@ -259,8 +310,12 @@ httpd_initialize(
 	{
 	hs->binding_hostname = (char*) 0;
 	hs->server_hostname = (char*) 0;
+#ifndef THTTPD_FOR_LITEOS
 	if ( gethostname( ghnbuf, sizeof(ghnbuf) ) < 0 )
 	    ghnbuf[0] = '\0';
+#else
+	strcpy(ghnbuf,THTTPD_HOSTNAME);
+#endif
 #ifdef SERVER_NAME_LIST
 	if ( ghnbuf[0] != '\0' )
 	    hs->server_hostname = hostname_map( ghnbuf );
@@ -2326,6 +2381,7 @@ httpd_parse_request( httpd_conn* hc )
     /* Expand all symbolic links in the filename.  This also gives us
     ** any trailing non-existing components, for pathinfo.
     */
+#ifndef THTTPD_FOR_LITEOS
     cp = expand_symlinks( hc->expnfilename, &pi, hc->hs->no_symlink_check, hc->tildemapped );
     if ( cp == (char*) 0 )
 	{
@@ -2336,7 +2392,7 @@ httpd_parse_request( httpd_conn* hc )
     (void) strcpy( hc->expnfilename, cp );
     httpd_realloc_str( &hc->pathinfo, &hc->maxpathinfo, strlen( pi ) );
     (void) strcpy( hc->pathinfo, pi );
-
+#endif
     /* Remove pathinfo stuff from the original filename too. */
     if ( hc->pathinfo[0] != '\0' )
 	{
@@ -2379,7 +2435,12 @@ httpd_parse_request( httpd_conn* hc )
 	    return -1;
 	    }
 	}
-
+#if THTTPD_FOR_LITEOS
+	int newExpLen = strlen( hc->hs->cwd ) + 1 + strlen( hc->origfilename ) + 1;
+    httpd_realloc_str(
+	&hc->expnfilename, &hc->maxexpnfilename, newExpLen );
+    snprintf(hc->expnfilename,newExpLen,"%s/%s",hc->hs->cwd,hc->origfilename);
+#endif
     return 0;
     }
 
@@ -2888,12 +2949,14 @@ mode  links    bytes  last-changed  name\n\
 		    case S_IFREG:  modestr[0] = '-'; break;
 		    case S_IFSOCK: modestr[0] = 's'; break;
 		    case S_IFLNK:  modestr[0] = 'l';
+#ifndef THTTPD_FOR_LITEOS
 		    linklen = readlink( name, lnk, sizeof(lnk) - 1 );
 		    if ( linklen != -1 )
 			{
 			lnk[linklen] = '\0';
 			linkprefix = " -&gt; ";
 			}
+#endif
 		    break;
 		    default:       modestr[0] = '?'; break;
 		    }
@@ -3657,6 +3720,7 @@ really_start_request( httpd_conn* hc, struct timeval* nowP )
     ** a file that's not set world-readable and yet somehow is
     ** readable by the HTTP server and therefore the *whole* world.
     */
+#ifndef THTTPD_FOR_LITEOS
     if ( ! ( hc->sb.st_mode & ( S_IROTH | S_IXOTH ) ) )
 	{
 	syslog(
@@ -3669,7 +3733,7 @@ really_start_request( httpd_conn* hc, struct timeval* nowP )
 	    hc->encodedurl );
 	return -1;
 	}
-
+#endif
     /* Is it a directory? */
     if ( S_ISDIR(hc->sb.st_mode) )
 	{
@@ -3706,6 +3770,11 @@ really_start_request( httpd_conn* hc, struct timeval* nowP )
 		indexname[0] = '\0';
 	    (void) strcat( indexname, index_names[i] );
 	    if ( stat( indexname, &hc->sb ) >= 0 )
+#if THTTPD_FOR_LITEOS
+                expnlen = strlen(indexname);
+                httpd_realloc_str(&hc->expnfilename, &hc->maxexpnfilename, expnlen);
+                (void) strcpy(hc->expnfilename, indexname);
+#endif
 		goto got_one;
 	    }
 
@@ -3749,6 +3818,7 @@ really_start_request( httpd_conn* hc, struct timeval* nowP )
 	/* Got an index file.  Expand symlinks again.  More pathinfo means
 	** something went wrong.
 	*/
+#ifndef THTTPD_FOR_LITEOS
 	cp = expand_symlinks( indexname, &pi, hc->hs->no_symlink_check, hc->tildemapped );
 	if ( cp == (char*) 0 || pi[0] != '\0' )
 	    {
@@ -3772,8 +3842,9 @@ really_start_request( httpd_conn* hc, struct timeval* nowP )
 		hc->encodedurl );
 	    return -1;
 	    }
+#endif
 	}
-
+	
 #ifdef AUTH_FILE
     /* Check authorization for this directory. */
     httpd_realloc_str( &dirname, &maxdirname, expnlen );
@@ -3828,6 +3899,7 @@ really_start_request( httpd_conn* hc, struct timeval* nowP )
 	 match( hc->hs->cgi_pattern, hc->expnfilename ) )
 	return cgi( hc );
 
+#ifndef THTTPD_FOR_LITEOS
     /* It's not CGI.  If it's executable or there's pathinfo, someone's
     ** trying to either serve or run a non-CGI file as CGI.   Either case
     ** is prohibited.
@@ -3843,6 +3915,7 @@ really_start_request( httpd_conn* hc, struct timeval* nowP )
 	    hc->encodedurl );
 	return -1;
 	}
+#endif
     if ( hc->pathinfo[0] != '\0' )
 	{
 	syslog(
@@ -3908,6 +3981,7 @@ httpd_start_request( httpd_conn* hc, struct timeval* nowP )
 static void
 make_log_entry( httpd_conn* hc, struct timeval* nowP )
     {
+#ifndef THTTPD_FOR_LITEOS
     char* ru;
     char url[305];
     char bytes[40];
@@ -3994,10 +4068,11 @@ make_log_entry( httpd_conn* hc, struct timeval* nowP )
 	}
     else
 	syslog( LOG_INFO,
-	    "%.80s - %.80s \"%.80s %.200s %.80s\" %d %s \"%.200s\" \"%.200s\"",
+	    "%.80s - %.80s \"%.80s %.200s %.80s\" %d %s \"%.200s\" \"%.200s\"\n",
 	    httpd_ntoa( &hc->client_addr ), ru,
 	    httpd_method_str( hc->method ), url, hc->protocol,
 	    hc->status, bytes, hc->referrer, hc->useragent );
+#endif
     }
 
 
diff --git a/thttpd-2.27/libhttpd.h b/thttpd-2.27/libhttpd.h
index 7805546..729ddd7 100644
--- a/thttpd-2.27/libhttpd.h
+++ b/thttpd-2.27/libhttpd.h
@@ -32,6 +32,7 @@
 #include <sys/time.h>
 #include <sys/param.h>
 #include <sys/socket.h>
+#include <sys/stat.h>
 #include <netinet/in.h>
 #include <arpa/inet.h>
 #include <netdb.h>
@@ -285,4 +286,6 @@ int httpd_write_fully( int fd, const char* buf, size_t nbytes );
 /* Generate debugging statistics syslog message. */
 void httpd_logstats( long secs );
 
+int ThttpdRun();
+
 #endif /* _LIBHTTPD_H_ */
diff --git a/thttpd-2.27/match.c b/thttpd-2.27/match.c
index 6facccb..1bffe76 100644
--- a/thttpd-2.27/match.c
+++ b/thttpd-2.27/match.c
@@ -35,7 +35,7 @@
 static int match_one( const char* pattern, int patternlen, const char* string );
 
 int
-match( const char* pattern, const char* string )
+thttpd_match( const char* pattern, const char* string )
     {
     const char* or;
 
diff --git a/thttpd-2.27/match.h b/thttpd-2.27/match.h
index 01c5f2a..2b5917d 100644
--- a/thttpd-2.27/match.h
+++ b/thttpd-2.27/match.h
@@ -31,6 +31,6 @@
 /* Simple shell-style filename pattern matcher.  Only does ? * and **, and
 ** multiple patterns separated by |.  Returns 1 or 0.
 */
-int match( const char* pattern, const char* string );
+int thttpd_match( const char* pattern, const char* string );
 
 #endif /* _MATCH_H_ */
diff --git a/thttpd-2.27/mime_encodings.h b/thttpd-2.27/mime_encodings.h
new file mode 100644
index 0000000..f12bd7f
--- a/thttpd-2.27/mime_encodings.h
+++ b/thttpd-2.27/mime_encodings.h
@@ -0,0 +1,3 @@
+{ "Z", 0, "compress", 0 },
+{ "gz", 0, "gzip", 0 },
+{ "uu", 0, "x-uuencode", 0 },
diff --git a/thttpd-2.27/mime_types.h b/thttpd-2.27/mime_types.h
new file mode 100644
index 0000000..15a93fa
--- a/thttpd-2.27/mime_types.h
+++ b/thttpd-2.27/mime_types.h
@@ -0,0 +1,193 @@
+{ "a", 0, "application/octet-stream", 0 },
+{ "aab", 0, "application/x-authorware-bin", 0 },
+{ "aam", 0, "application/x-authorware-map", 0 },
+{ "aas", 0, "application/x-authorware-seg", 0 },
+{ "ai", 0, "application/postscript", 0 },
+{ "aif", 0, "audio/x-aiff", 0 },
+{ "aifc", 0, "audio/x-aiff", 0 },
+{ "aiff", 0, "audio/x-aiff", 0 },
+{ "asc", 0, "text/plain; charset=%s", 0 },
+{ "asf", 0, "video/x-ms-asf", 0 },
+{ "asx", 0, "video/x-ms-asf", 0 },
+{ "au", 0, "audio/basic", 0 },
+{ "avi", 0, "video/x-msvideo", 0 },
+{ "bcpio", 0, "application/x-bcpio", 0 },
+{ "bin", 0, "application/octet-stream", 0 },
+{ "bmp", 0, "image/bmp", 0 },
+{ "cdf", 0, "application/x-netcdf", 0 },
+{ "class", 0, "application/x-java-vm", 0 },
+{ "cpio", 0, "application/x-cpio", 0 },
+{ "cpt", 0, "application/mac-compactpro", 0 },
+{ "crl", 0, "application/x-pkcs7-crl", 0 },
+{ "crt", 0, "application/x-x509-ca-cert", 0 },
+{ "csh", 0, "application/x-csh", 0 },
+{ "css", 0, "text/css; charset=%s", 0 },
+{ "dcr", 0, "application/x-director", 0 },
+{ "dir", 0, "application/x-director", 0 },
+{ "djv", 0, "image/vnd.djvu", 0 },
+{ "djvu", 0, "image/vnd.djvu", 0 },
+{ "dll", 0, "application/octet-stream", 0 },
+{ "dms", 0, "application/octet-stream", 0 },
+{ "doc", 0, "application/msword", 0 },
+{ "dtd", 0, "text/xml; charset=%s", 0 },
+{ "dump", 0, "application/octet-stream", 0 },
+{ "dvi", 0, "application/x-dvi", 0 },
+{ "dxr", 0, "application/x-director", 0 },
+{ "eps", 0, "application/postscript", 0 },
+{ "etx", 0, "text/x-setext", 0 },
+{ "exe", 0, "application/octet-stream", 0 },
+{ "ez", 0, "application/andrew-inset", 0 },
+{ "fgd", 0, "application/x-director", 0 },
+{ "fh", 0, "image/x-freehand", 0 },
+{ "fh4", 0, "image/x-freehand", 0 },
+{ "fh5", 0, "image/x-freehand", 0 },
+{ "fh7", 0, "image/x-freehand", 0 },
+{ "fhc", 0, "image/x-freehand", 0 },
+{ "gif", 0, "image/gif", 0 },
+{ "gtar", 0, "application/x-gtar", 0 },
+{ "hdf", 0, "application/x-hdf", 0 },
+{ "hqx", 0, "application/mac-binhex40", 0 },
+{ "htm", 0, "text/html; charset=%s", 0 },
+{ "html", 0, "text/html; charset=%s", 0 },
+{ "ice", 0, "x-conference/x-cooltalk", 0 },
+{ "ief", 0, "image/ief", 0 },
+{ "iges", 0, "model/iges", 0 },
+{ "igs", 0, "model/iges", 0 },
+{ "iv", 0, "application/x-inventor", 0 },
+{ "jar", 0, "application/x-java-archive", 0 },
+{ "jfif", 0, "image/jpeg", 0 },
+{ "jpe", 0, "image/jpeg", 0 },
+{ "jpeg", 0, "image/jpeg", 0 },
+{ "jpg", 0, "image/jpeg", 0 },
+{ "js", 0, "application/x-javascript", 0 },
+{ "kar", 0, "audio/midi", 0 },
+{ "kml", 0, "application/vnd.google-earth.kml+xml", 0 },
+{ "kmz", 0, "application/vnd.google-earth.kmz", 0 },
+{ "latex", 0, "application/x-latex", 0 },
+{ "lha", 0, "application/octet-stream", 0 },
+{ "loc", 0, "application/xml-loc", 0 },
+{ "lzh", 0, "application/octet-stream", 0 },
+{ "m3u", 0, "audio/x-mpegurl", 0 },
+{ "man", 0, "application/x-troff-man", 0 },
+{ "mathml", 0, "application/mathml+xml", 0 },
+{ "me", 0, "application/x-troff-me", 0 },
+{ "mesh", 0, "model/mesh", 0 },
+{ "mid", 0, "audio/midi", 0 },
+{ "midi", 0, "audio/midi", 0 },
+{ "mif", 0, "application/vnd.mif", 0 },
+{ "mime", 0, "message/rfc822", 0 },
+{ "mml", 0, "application/mathml+xml", 0 },
+{ "mov", 0, "video/quicktime", 0 },
+{ "movie", 0, "video/x-sgi-movie", 0 },
+{ "mp2", 0, "audio/mpeg", 0 },
+{ "mp3", 0, "audio/mpeg", 0 },
+{ "mp4", 0, "video/mp4", 0 },
+{ "mpe", 0, "video/mpeg", 0 },
+{ "mpeg", 0, "video/mpeg", 0 },
+{ "mpg", 0, "video/mpeg", 0 },
+{ "mpga", 0, "audio/mpeg", 0 },
+{ "ms", 0, "application/x-troff-ms", 0 },
+{ "msh", 0, "model/mesh", 0 },
+{ "mv", 0, "video/x-sgi-movie", 0 },
+{ "mxu", 0, "video/vnd.mpegurl", 0 },
+{ "nc", 0, "application/x-netcdf", 0 },
+{ "o", 0, "application/octet-stream", 0 },
+{ "oda", 0, "application/oda", 0 },
+{ "ogg", 0, "application/ogg", 0 },
+{ "pac", 0, "application/x-ns-proxy-autoconfig", 0 },
+{ "pbm", 0, "image/x-portable-bitmap", 0 },
+{ "pdb", 0, "chemical/x-pdb", 0 },
+{ "pdf", 0, "application/pdf", 0 },
+{ "pgm", 0, "image/x-portable-graymap", 0 },
+{ "pgn", 0, "application/x-chess-pgn", 0 },
+{ "png", 0, "image/png", 0 },
+{ "pnm", 0, "image/x-portable-anymap", 0 },
+{ "ppm", 0, "image/x-portable-pixmap", 0 },
+{ "ppt", 0, "application/vnd.ms-powerpoint", 0 },
+{ "ps", 0, "application/postscript", 0 },
+{ "qt", 0, "video/quicktime", 0 },
+{ "ra", 0, "audio/x-realaudio", 0 },
+{ "ram", 0, "audio/x-pn-realaudio", 0 },
+{ "ras", 0, "image/x-cmu-raster", 0 },
+{ "rdf", 0, "application/rdf+xml", 0 },
+{ "rgb", 0, "image/x-rgb", 0 },
+{ "rm", 0, "audio/x-pn-realaudio", 0 },
+{ "roff", 0, "application/x-troff", 0 },
+{ "rpm", 0, "audio/x-pn-realaudio-plugin", 0 },
+{ "rss", 0, "application/rss+xml", 0 },
+{ "rtf", 0, "text/rtf; charset=%s", 0 },
+{ "rtx", 0, "text/richtext; charset=%s", 0 },
+{ "sgm", 0, "text/sgml; charset=%s", 0 },
+{ "sgml", 0, "text/sgml; charset=%s", 0 },
+{ "sh", 0, "application/x-sh", 0 },
+{ "shar", 0, "application/x-shar", 0 },
+{ "silo", 0, "model/mesh", 0 },
+{ "sit", 0, "application/x-stuffit", 0 },
+{ "skd", 0, "application/x-koan", 0 },
+{ "skm", 0, "application/x-koan", 0 },
+{ "skp", 0, "application/x-koan", 0 },
+{ "skt", 0, "application/x-koan", 0 },
+{ "smi", 0, "application/smil", 0 },
+{ "smil", 0, "application/smil", 0 },
+{ "snd", 0, "audio/basic", 0 },
+{ "so", 0, "application/octet-stream", 0 },
+{ "spl", 0, "application/x-futuresplash", 0 },
+{ "src", 0, "application/x-wais-source", 0 },
+{ "stc", 0, "application/vnd.sun.xml.calc.template", 0 },
+{ "std", 0, "application/vnd.sun.xml.draw.template", 0 },
+{ "sti", 0, "application/vnd.sun.xml.impress.template", 0 },
+{ "stw", 0, "application/vnd.sun.xml.writer.template", 0 },
+{ "sv4cpio", 0, "application/x-sv4cpio", 0 },
+{ "sv4crc", 0, "application/x-sv4crc", 0 },
+{ "svg", 0, "image/svg+xml", 0 },
+{ "svgz", 0, "image/svg+xml", 0 },
+{ "swf", 0, "application/x-shockwave-flash", 0 },
+{ "sxc", 0, "application/vnd.sun.xml.calc", 0 },
+{ "sxd", 0, "application/vnd.sun.xml.draw", 0 },
+{ "sxg", 0, "application/vnd.sun.xml.writer.global", 0 },
+{ "sxi", 0, "application/vnd.sun.xml.impress", 0 },
+{ "sxm", 0, "application/vnd.sun.xml.math", 0 },
+{ "sxw", 0, "application/vnd.sun.xml.writer", 0 },
+{ "t", 0, "application/x-troff", 0 },
+{ "tar", 0, "application/x-tar", 0 },
+{ "tcl", 0, "application/x-tcl", 0 },
+{ "tex", 0, "application/x-tex", 0 },
+{ "texi", 0, "application/x-texinfo", 0 },
+{ "texinfo", 0, "application/x-texinfo", 0 },
+{ "tif", 0, "image/tiff", 0 },
+{ "tiff", 0, "image/tiff", 0 },
+{ "tr", 0, "application/x-troff", 0 },
+{ "tsp", 0, "application/dsptype", 0 },
+{ "tsv", 0, "text/tab-separated-values; charset=%s", 0 },
+{ "txt", 0, "text/plain; charset=%s", 0 },
+{ "ustar", 0, "application/x-ustar", 0 },
+{ "vcd", 0, "application/x-cdlink", 0 },
+{ "vrml", 0, "model/vrml", 0 },
+{ "vx", 0, "video/x-rad-screenplay", 0 },
+{ "wav", 0, "audio/x-wav", 0 },
+{ "wax", 0, "audio/x-ms-wax", 0 },
+{ "wbmp", 0, "image/vnd.wap.wbmp", 0 },
+{ "wbxml", 0, "application/vnd.wap.wbxml", 0 },
+{ "wm", 0, "video/x-ms-wm", 0 },
+{ "wma", 0, "audio/x-ms-wma", 0 },
+{ "wmd", 0, "application/x-ms-wmd", 0 },
+{ "wml", 0, "text/vnd.wap.wml", 0 },
+{ "wmlc", 0, "application/vnd.wap.wmlc", 0 },
+{ "wmls", 0, "text/vnd.wap.wmlscript", 0 },
+{ "wmlsc", 0, "application/vnd.wap.wmlscriptc", 0 },
+{ "wmv", 0, "video/x-ms-wmv", 0 },
+{ "wmx", 0, "video/x-ms-wmx", 0 },
+{ "wmz", 0, "application/x-ms-wmz", 0 },
+{ "wrl", 0, "model/vrml", 0 },
+{ "wsrc", 0, "application/x-wais-source", 0 },
+{ "wvx", 0, "video/x-ms-wvx", 0 },
+{ "xbm", 0, "image/x-xbitmap", 0 },
+{ "xht", 0, "application/xhtml+xml; charset=%s", 0 },
+{ "xhtml", 0, "application/xhtml+xml; charset=%s", 0 },
+{ "xls", 0, "application/vnd.ms-excel", 0 },
+{ "xml", 0, "text/xml; charset=%s", 0 },
+{ "xpm", 0, "image/x-xpixmap", 0 },
+{ "xsl", 0, "text/xml; charset=%s", 0 },
+{ "xwd", 0, "image/x-xwindowdump", 0 },
+{ "xyz", 0, "chemical/x-xyz", 0 },
+{ "zip", 0, "application/zip", 0 },
diff --git a/thttpd-2.27/strerror.c b/thttpd-2.27/strerror.c
index 6a161e6..6a76c6e 100644
--- a/thttpd-2.27/strerror.c
+++ b/thttpd-2.27/strerror.c
@@ -18,7 +18,7 @@
 #if defined(LIBC_SCCS) && !defined(lint)
 static char sccsid[] = "@(#)strerror.c  5.1 (Berkeley) 4/9/89";
 #endif /* LIBC_SCCS and not lint */
-
+#include "config.h"
 #include <sys/types.h>
 
 #include <stdio.h>
@@ -30,9 +30,10 @@ strerror(errnum)
 	extern int sys_nerr;
 	extern char *sys_errlist[];
 	static char ebuf[20];
-
+#ifndef THTTPD_FOR_LITEOS
 	if ((unsigned int)errnum < sys_nerr)
 		return(sys_errlist[errnum]);
+#endif
 	(void)sprintf(ebuf, "Unknown error: %d", errnum);
 	return(ebuf);
 }
diff --git a/thttpd-2.27/thttpd.c b/thttpd-2.27/thttpd.c
index 3be7546..83d826d 100644
--- a/thttpd-2.27/thttpd.c
+++ b/thttpd-2.27/thttpd.c
@@ -33,7 +33,9 @@
 #include <sys/types.h>
 #include <sys/time.h>
 #include <sys/stat.h>
+#ifndef THTTPD_FOR_LITEOS
 #include <sys/wait.h>
+#endif
 #include <sys/uio.h>
 
 #include <errno.h>
@@ -59,6 +61,7 @@
 #include "mmc.h"
 #include "timers.h"
 #include "match.h"
+#include "time.h"
 
 #ifndef SHUT_WR
 #define SHUT_WR 1
@@ -101,8 +104,6 @@ static throttletab* throttles;
 static int numthrottles, maxthrottles;
 
 #define THROTTLE_NOLIMIT -1
-
-
 typedef struct {
     int conn_state;
     int next_free_connect;
@@ -170,6 +171,9 @@ static void show_stats( ClientData client_data, struct timeval* nowP );
 static void logstats( struct timeval* nowP );
 static void thttpd_logstats( long secs );
 
+#ifdef THTTPD_FOR_LITEOS
+#define exit(x) return(x)
+#endif
 
 /* SIGTERM and SIGINT say to exit immediately. */
 static void
@@ -188,6 +192,7 @@ handle_term( int sig )
 static void
 handle_chld( int sig )
     {
+#ifndef THTTPD_FOR_LITEOS
     const int oerrno = errno;
     pid_t pid;
     int status;
@@ -234,6 +239,7 @@ handle_chld( int sig )
 
     /* Restore previous errno. */
     errno = oerrno;
+#endif
     }
 
 
@@ -241,6 +247,7 @@ handle_chld( int sig )
 static void
 handle_hup( int sig )
     {
+#ifndef THTTPD_FOR_LITEOS
     const int oerrno = errno;
 
 #ifndef HAVE_SIGSET
@@ -253,6 +260,7 @@ handle_hup( int sig )
 
     /* Restore previous errno. */
     errno = oerrno;
+#endif
     }
 
 
@@ -330,6 +338,7 @@ handle_alrm( int sig )
 static void
 re_open_logfile( void )
     {
+#ifndef THTTPD_FOR_LITEOS
     FILE* logfp;
 
     if ( no_log || hs == (httpd_server*) 0 )
@@ -348,18 +357,27 @@ re_open_logfile( void )
 	(void) fcntl( fileno( logfp ), F_SETFD, 1 );
 	httpd_set_logfp( hs, logfp );
 	}
+#endif
     }
 
 
 int
+#if THTTPD_FOR_LITEOS
+ThttpdRun()
+#else
 main( int argc, char** argv )
+#endif
     {
     char* cp;
     struct passwd* pwd;
     uid_t uid = 32767;
     gid_t gid = 32767;
+#if THTTPD_FOR_LITEOS
+    char cwd[MAXPATHLEN+1]=THTTP_WWWROOT_DIR;
+#else
     char cwd[MAXPATHLEN+1];
-    FILE* logfp;
+#endif
+    FILE* logfp = 0;
     int num_ready;
     int cnum;
     connecttab* c;
@@ -368,7 +386,7 @@ main( int argc, char** argv )
     httpd_sockaddr sa6;
     int gotv4, gotv6;
     struct timeval tv;
-
+#ifndef THTTPD_FOR_LITEOS
     argv0 = argv[0];
 
     cp = strrchr( argv0, '/' );
@@ -383,7 +401,6 @@ main( int argc, char** argv )
 
     /* Read zone info now, in case we chroot(). */
     tzset();
-
     /* Look up hostname now, in case we chroot(). */
     lookup_hostname( &sa4, sizeof(sa4), &gotv4, &sa6, sizeof(sa6), &gotv6 );
     if ( ! ( gotv4 || gotv6 ) )
@@ -392,6 +409,13 @@ main( int argc, char** argv )
 	(void) fprintf( stderr, "%s: can't find any valid address\n", argv0 );
 	exit( 1 );
 	}
+#else
+    sa4.sa_in.sin_family      = AF_INET;
+    sa4.sa_in.sin_port        = htons(THTTPD_PORT);
+    sa4.sa_in.sin_addr.s_addr = htonl(THTTPD_IPADDR);
+    gotv4=1;
+    gotv6=0;
+#endif
 
     /* Throttle file. */
     numthrottles = 0;
@@ -400,6 +424,7 @@ main( int argc, char** argv )
     if ( throttlefile != (char*) 0 )
 	read_throttlefile( throttlefile );
 
+#ifndef THTTPD_FOR_LITEOS
     /* If we're root and we're going to become another user, get the uid/gid
     ** now.
     */
@@ -543,6 +568,7 @@ main( int argc, char** argv )
 	(void) fprintf( pidfp, "%d\n", (int) getpid() );
 	(void) fclose( pidfp );
 	}
+#endif
 
     /* Initialize the fdwatch package.  Have to do this before chroot,
     ** if /dev/poll is used.
@@ -554,7 +580,7 @@ main( int argc, char** argv )
 	exit( 1 );
 	}
     max_connects -= SPARE_FDS;
-
+#ifndef THTTPD_FOR_LITEOS
     /* Chroot if requested. */
     if ( do_chroot )
 	{
@@ -605,7 +631,6 @@ main( int argc, char** argv )
 	    exit( 1 );
 	    }
 	}
-
     /* Set up to catch signals. */
 #ifdef HAVE_SIGSET
     (void) sigset( SIGTERM, handle_term );
@@ -626,10 +651,20 @@ main( int argc, char** argv )
     (void) signal( SIGUSR2, handle_usr2 );
     (void) signal( SIGALRM, handle_alrm );
 #endif /* HAVE_SIGSET */
+#endif
+
+#if THTTPD_FOR_LITEOS
+    port = THTTPD_PORT;
+    no_symlink_check = 0;
+#endif
+
     got_hup = 0;
     got_usr1 = 0;
     watchdog_flag = 0;
+
+#ifndef THTTPD_FOR_LITEOS
     (void) alarm( OCCASIONAL_TIME * 3 );
+#endif
 
     /* Initialize the timer package. */
     tmr_init();
@@ -679,7 +714,7 @@ main( int argc, char** argv )
     stats_connections = 0;
     stats_bytes = 0;
     stats_simultaneous = 0;
-
+#ifndef THTTPD_FOR_LITEOS
     /* If we're root, try to become someone else. */
     if ( getuid() == 0 )
 	{
@@ -714,6 +749,7 @@ main( int argc, char** argv )
 		LOG_WARNING,
 		"started as root without requesting chroot(), warning only" );
 	}
+#endif
 
     /* Initialize our connections table. */
     connects = NEW( connecttab, max_connects );
@@ -798,10 +834,12 @@ main( int argc, char** argv )
 	    if ( c == (connecttab*) 0 )
 		continue;
 	    hc = c->hc;
-	    if ( ! fdwatch_check_fd( hc->conn_fd ) )
+	    if ( ! fdwatch_check_fd( hc->conn_fd ) ){
 		/* Something went wrong. */
+#ifndef THTTPD_FOR_LITEOS
 		clear_connection( c, &tv );
-	    else
+#endif
+	    } else
 		switch ( c->conn_state )
 		    {
 		    case CNST_READING: handle_read( c, &tv ); break;
@@ -1547,7 +1585,11 @@ handle_newconnect( struct timeval* tvP, int listen_fd )
 
 	    /* No more connections to accept for now. */
 	    case GC_NO_MORE:
+#if THTTPD_FOR_LITEOS
+	    return 0;
+#else
 	    return 1;
+#endif
 	    }
 	c->conn_state = CNST_READING;
 	/* Pop it off the free list. */
@@ -1880,7 +1922,7 @@ check_throttles( connecttab* c )
     c->max_limit = c->min_limit = THROTTLE_NOLIMIT;
     for ( tnum = 0; tnum < numthrottles && c->numtnums < MAXTHROTTLENUMS;
 	  ++tnum )
-	if ( match( throttles[tnum].pattern, c->hc->expnfilename ) )
+	if ( thttpd_match( throttles[tnum].pattern, c->hc->expnfilename ) )
 	    {
 	    /* If we're way over the limit, don't even start. */
 	    if ( throttles[tnum].rate > throttles[tnum].max_limit * 2 )
